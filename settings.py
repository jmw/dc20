# -*- encoding: utf-8 -*-
from pathlib import Path
from datetime import date
from decimal import Decimal

from django.urls import reverse_lazy

from debconf.settings import *

TIME_ZONE = 'Asia/Jerusalem'
USE_L10N = False
TIME_FORMAT = 'G:i'
SHORT_DATE_FORMAT = 'Y-m-d'
DATETIME_FORMAT = 'Y-m-d H:i:s'
SHORT_DATETIME_FORMAT = 'Y-m-d H:i'

try:
    from localsettings import *
except ImportError:
    pass

root = Path(__file__).parent

INSTALLED_APPS = (
    'badges',
    'debconf',
    'dc20',
    'exports',
    'news',
    'django_countries',
    'paypal.standard.ipn',
    'bursary',
    'front_desk',
    'invoices',
    'register',
    'volunteers',
) + INSTALLED_APPS

STATIC_ROOT = str(root / 'localstatic/')

STATICFILES_DIRS = (
    str(root / 'static'),
)

STATICFILES_STORAGE = (
    'django.contrib.staticfiles.storage.ManifestStaticFilesStorage')

TEMPLATES[0]['DIRS'] = TEMPLATES[0]['DIRS'] + (
    str(root / 'templates'),
)
TEMPLATES[0]['OPTIONS']['context_processors'] += ('dc20.context_processors.expose_wafer_talks',)


WAFER_MENUS += (
    {
        'menu': 'about',
        'label': 'About',
        'items': [
            {
                'menu': 'coc',
                'label': 'Code of Conduct',
                'url': reverse_lazy('wafer_page', args=('about/coc',)),
            },
            {
                'menu': 'debconf',
                'label': 'DebConf',
                'url': reverse_lazy('wafer_page', args=('about/debconf',)),
            },
            {
                'menu': 'debian',
                'label': 'Debian',
                'url': reverse_lazy('wafer_page', args=('about/debian',)),
            },
            {
                'menu': 'accommodation',
                'label': 'Accommodation',
                'url': reverse_lazy('wafer_page', args=('about/accommodation',)),
            },
            {
                'menu': 'bursaries',
                'label': 'Bursaries',
                'url': reverse_lazy('wafer_page', args=('about/bursaries',)),
            },
            {
                'menu': 'cheese-and-wine-party',
                'label': 'Cheese and Wine Party',
                'url': reverse_lazy('wafer_page', args=('about/cheese-and-wine-party',)),
            },
            {
                'menu': 'child-care',
                'label': 'Child care',
                'url': reverse_lazy('wafer_page', args=('about/childcare',)),
            },
            {
                'menu': 'confdinner',
                'label': 'Conference Dinner',
                'url': reverse_lazy('wafer_page', args=('about/confdinner',)),
            },
            {
                'menu': 'haifa',
                'label': 'Haifa',
                'url': reverse_lazy('wafer_page', args=('about/haifa',)),
            },
            {
                'menu': 'faq',
                'label': 'FAQ',
                'url': 'https://wiki.debian.org/DebConf/20/Faq',
            },
            {
                'menu': 'registration_information',
                'label': 'Registration Information',
                'url': reverse_lazy('wafer_page', args=('about/registration',)),
            },
            {
                'menu': 'venue',
                'label': 'Venue',
                'url': reverse_lazy('wafer_page', args=('about/venue',)),
            },
            {
                'menu': 'visas',
                'label': 'Visas',
                'url': reverse_lazy('wafer_page', args=('about/visas',)),
            },
            {
                'menu': 'contact_link',
                'label': 'Contact us',
                'url': reverse_lazy('wafer_page', args=('contact',)),
            },
        ],
    },
    {
        'menu': 'sponsors_index',
        'label': 'Sponsors',
        'items': [
            {
                'menu': 'become_sponsor',
                'label': 'Become a Sponsor',
                'url': reverse_lazy('wafer_page', args=('sponsors/become-a-sponsor',)),
            },
            {
                'menu': 'sponsors',
                'label': 'Our Sponsors',
                'url': reverse_lazy('wafer_sponsors'),
            },
        ],
    },
    {
        'menu': 'schedule_index',
        'label': 'Schedule',
        'items': [
            {
                'menu': 'cfp',
                'label': 'Call for Proposals',
                'url': reverse_lazy('wafer_page', args=('cfp',)),
            },
            {
                'menu': 'confirmed_talks',
                'label': 'Confirmed Talks',
                'url': reverse_lazy('wafer_users_talks'),
            },
            {
                'menu': 'important-dates',
                'label': 'Important Dates',
                'url': reverse_lazy('wafer_page', args=('schedule/important-dates',)),
            },
            {
                'menu': 'mobile_schedule',
                'label': 'Mobile-Friendly Schedule',
                'url': reverse_lazy('wafer_page', args=('schedule/mobile',)),
            },
            {
                'menu': 'schedule',
                'label': 'Schedule',
                'url': reverse_lazy('wafer_full_schedule'),
            },
        ],
    },
    {
        'menu': 'wiki_link',
        'label': 'Wiki',
        'url': 'https://wiki.debian.org/DebConf/20',
    },
    # Once the conference starts:
    #{
    #    'menu': 'volunteers',
    #    'label': 'Volunteer!',
    #    'url': reverse_lazy('wafer_tasks'),
    #},
)

WAFER_DYNAMIC_MENUS = ()

ROOT_URLCONF = 'urls'

CRISPY_TEMPLATE_PACK = 'bootstrap4'
CRISPY_FAIL_SILENTLY = not DEBUG

MARKITUP_FILTER = ('markdown.markdown', {
    'extensions': [
        'markdown.extensions.smarty',
        'markdown.extensions.tables',
        'markdown.extensions.toc',
        'mdx_linkify.mdx_linkify',
        'dc20.markdown',
    ],
    'output_format': 'html5',
    'safe_mode': False,
})
MARKITUP_SET = 'markitup/sets/markdown/'
JQUERY_URL = 'js/jquery.js'

DEFAULT_FROM_EMAIL = 'registration@debconf.org'
REGISTRATION_DEFAULT_FROM_EMAIL = 'noreply@debconf.org'

WAFER_REGISTRATION_MODE = 'custom'
WAFER_USER_IS_REGISTERED = 'register.models.user_is_registered'

WAFER_VIDEO_REVIEWER = False
WAFER_VIDEO_LICENSE = 'DebConf Video License (MIT-like)'
WAFER_VIDEO_LICENSE_URL = 'https://video.debian.net/LICENSE'

WAFER_PUBLIC_ATTENDEE_LIST = False

DEBCONF_CITY = 'Haifa'
DEBCONF_NAME = 'DebConf 20'
DEBCONF_PAID_ACCOMMODATION = False
# FIXME
DEBCONF_CONFIRMATION_DEADLINE = date(2020, 7, 1)
DEBCONF_BURSARY_DEADLINE = date(2020, 4, 1)
DEBCONF_BURSARY_ACCEPTANCE_DEADLINE = date(2019, 7, 1)
DEBCONF_LOCAL_CURRENCY = 'ILS'
DEBCONF_LOCAL_CURRENCY_SYMBOL = '₪'
DEBCONF_LOCAL_CURRENCY_RATE = Decimal('3.50')
DEBCONF_BREAKFAST = False

DEBCONF_DATES = (
    # Conference part, start date, end date
    # FIXME
    ('DebCamp', date(2020, 8, 16), date(2020, 8, 22)),
    ('DebConf', date(2020, 8, 23), date(2020, 8, 29)),
)
# FIXME
DEBCONF_CONFERENCE_DINNER_DAY = date(2020, 8, 27)

VOLUNTEERS_FIRST_DAY = date(2020, 8, 14)
VOLUNTEERS_LAST_DAY = date(2020, 8, 30)

# FIXME
DEBCONF_T_SHIRT_SIZES = (
    ('', 'No T-shirt'),
    ('', '—'),
)
DEBCONF_SHOE_SIZES = ()

PAGE_DIR = '%s/' % (root / 'pages')
NEWS_DIR = '%s/' % (root / 'news' / 'stories')
SPONSORS_DIR = '%s/' % (root / 'sponsors')

TRACKS_FILE = str(root / 'tracks.yml')
TALK_TYPES_FILE = str(root / 'talk_types.yml')

BAKERY_VIEWS += (
    'dc20.views.IndexView',
    'news.views.NewsItemView',
    'news.views.NewsFeedView',
    'news.views.NewsRSSView',
    'news.views.NewsAtomView',
)
BUILD_DIR = '%s/' % (root / 'static_mirror')
