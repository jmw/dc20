---
name: About DebConf
---
# About DebConf

DebConf is the annual conference for [Debian](http://debian.org) contributors
and users interested in [improving Debian](https://www.debian.org/intro/help).
[Previous Debian conferences](https://www.debconf.org) have featured speakers
and attendees from all around the world. [DebConf19][] took place in Curitiba,
Brazil and was attended by 382 participants from 50 countries.

[DebConf19]: https://debconf19.debconf.org/

**DebConf20 is taking place in Haifa, Israel from 23-8-2020 to 29-8-2020.**

It is being preceded by DebCamp (16-8-2020 to 22-8-2020).

We look forward to seeing you in Haifa!

<form method="POST" action="https://lists.debian.org/cgi-bin/subscribe.pl">
  <fieldset>
    <legend>Register to the debconf-announce mailing list</legend>
    <label for="user_email">Your email address:</label>
    <input name="user_email" size="40" value="" type="Text">
    <input type="hidden" name="list" value="debconf-announce">
    <input name="action" value="Subscribe" type="Submit">
  </fieldset>
</form>

## The Venue

[University of Haifa](https://en.wikipedia.org/wiki/University_of_Haifa)
(specifically: its main campus) is located on the top of Mt. Carmel above Haifa.

The conference is to be held at the [main building](https://www.openstreetmap.org/way/183281539) of the university
(the one at the foot of the tall Eshkol tower), in various halls and rooms in the Eastern part of its "7th" floor
(the level of the entrance from the street).

There are two halls (205 and 208 seats) and various smaller classes nearby.

## Dormitories

We will use the [University dormitories](https://www.openstreetmap.org/query?lat=32.75945&lon=35.02360).

The apartments are for six people each with a common area and a kitchen (and a wi-fi router).

There are two types of dormitories:

 * Federman: in each apartment 3 rooms of two beds. Shared bathrooms and toilets.
 * Talia: in each apartment there are 6 rooms of one bed with toilets. Shared bathrooms.

They are roughly 500 meters from the venue.

## Codes of Conduct and Community Team

Please see the [Code of Conduct](../coc/) page for more information.
