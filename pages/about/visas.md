---
name: Visas
---
# Visas

Israel has visa exemptions for visitors for several countries, most DebConf attendees will not require a visa to attend DebConf.

The following countries citizens are exempt from a visa: All of the European Union, Albania, Andorra, Argentina, Australia, Bahamas, Barbados, Belarus, Belize,  Botswana, Brazil, Brexit, Canada, Central African Republic, Chile, Colombia, Costa Rica, Dominica, Dominican Republic, Ecuador, El Salvador, Eswatini, Fiji, Georgia, Grenada, Guatemala, Haiti, Honduras, Hong Kong, Iceland, Jamaica, Japan, Lesotho, Liechtenstein, Macao, Malawi, Mauritius, Mexico, Micronesia, Moldova, Monaco, Mongolia, Montenegro, Nauru, New Zealand, North Macedonia, Norway, Palau, Panama, Papua New Guinea, Paraguay, Peru, Philippines, Russia, Saint Kitts and Nevis, Saint Lucia, Saint Vincent and the Grenadines, San Marino, Serbia, Singapore, Solomon Islands, South Africa, South Korea, Suriname, Switzerland, Taiwan, Tonga, Trinidad and Tobago, Ukraine, United Kingdom, United States, Uruguay, Vanuatu, Vatican City.

We are aware of Debian Developers living in the following countries, that would need to apply for a visa, to attend DebConf:

 * Bosnia-Herzegovina
 * China
 * Cuba
 * Egypt
 * India
 * Madagascar
 * Thailand
 * Turkey
 * Venezuela

Citizens of Kosovo also require a visa to visit Israel.
 
If your country of citizenship is not listed above, refer to the "national" column of the [Israel Ministry of Foreign Affairs' list](https://en.wikipedia.org/wiki/Visa_policy_of_Israel) or [Wikipedia's page on the topic](https://mfa.gov.il/MFA/ConsularServices/Documents/VisaRequirements-Tourists.pdf).

If you require a visitor visa, read the [application process](https://www.israelvisa.in/how-to-apply) and apply for a B/2 visa. Refer to the [different visa types](https://mfa.gov.il/MFA/ConsularServices/Pages/Visas.aspx) issued by any Israeli embassy. Israeli visa application takes 2-3 months.  
You need to present the following documents at your nearest [Israeli embassy or consulate](https://embassies.gov.il/Pages/IsraeliMissionsAroundTheWorld.aspx): 

 * A valid passport (original and copy)
 * A completed visa application
 * Proof of the applicant's sufficient financial means for the visit to Israel (such as bank statements from the last three months)
 * A return ticket
 * 2 passport photographs (5x5 cm)
 * Payment of the visa application fee

For future travel plans you may have, some Arab countries listed [here](https://igoogledisrael.com/can-i-visit-arab-countries-with-an-israeli-visa-stamp-in-my-passport/) do not allow visitors with Israeli stamp on their passport to enter their country. Because of this Israel doesn't stamp passports for visa-exemptions, any more. But getting a visa will result in a stamp in your passport.

If you need any help obtaining a visa, please email the visa team visa@debconf.org with your details. 
If necessary, we have contacts with the Ministry of Foreign affairs that we can speak to.
