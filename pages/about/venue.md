---
name: The Conference Venue
---
# The Conference Venue

## Campus

The Conference will be held inside the main campus at the [University of Haifa](https://en.wikipedia.org/wiki/University_of_Haifa) located on the top of Mt. Carmel above Haifa.

The University of Haifa, founded in 1963, is Israel's fourth university. It has the largest university library in the country.

There are several tourist attractions within a few kilometers of the University: The Hecht Museum, Hanging Bridge at Nesher Park, Carmel National Park, etc.

## Address

Abba Khoushy Ave 199  
Haifa  
3498838  
Israel  

## Wikipedia

* [University of Haifa](https://en.wikipedia.org/wiki/University_of_Haifa) (English)
* [אוניברסיטת חיפה](https://he.wikipedia.org/wiki/%D7%90%D7%95%D7%A0%D7%99%D7%91%D7%A8%D7%A1%D7%99%D7%98%D7%AA_%D7%97%D7%99%D7%A4%D7%94) (Hebrew)


## Maps

[OpenStreetMap](https://www.openstreetmap.org/way/55001924#map=16/32.7612/35.0202)

[Google Maps](https://www.google.com/maps/place/University+of+Haifa/@32.7614341,35.0173297,17z/data=!3m1!4b1!4m5!3m4!1s0x151dbab83f7be8ab:0x475fdbcf84359ab8!8m2!3d32.7614296!4d35.0195184)

## Rooms

The conference venue will be on the "7th" floor (the level of the entrance from the street) of the [main building](https://www.openstreetmap.org/way/55001924#map=16/32.7612/35.0202), with all of the rooms close together. There will be 2 larger talk rooms (~200 seats) and a smaller one (~60 seats), 3 BoF rooms, 1 noisy hacklab on the 6th floor and 2 other smaller hacklabs, 1 orga room, 1 video team room, 1 front desk, 1 server room and 1 private meeting room (~12 seats).

## Getting to the Venue

Flights land at Ben Gurion airport (TLV, 20 KM from Tel Aviv). From there the best option would be taking the Train to Haifa. It's about 1:20 to Haifa (Tel Aviv to haifa is 1 hour).

From Hafia's Hof Hacarmel train station you go (50 meters) to the central bus station (same name) to take bus 146 to the university. Bus stops: - Eshkol Tower stop for venue / front-desk - Multi-Purpose Building stop for the dormitories.


