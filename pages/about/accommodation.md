---
name: Accommodation
---

# Accommodation

We will use the [University dormitories](https://www.openstreetmap.org/query?lat=32.75945&lon=35.02360).

The apartments are for six people each with a common area and a kitchen (and a wi-fi router).

There are two types of dormitories:

 * Federman: in each apartment 3 rooms of two beds. Shared bathrooms and toilets.
 * Talia: in each apartment there are 6 rooms of one bed with toilets. Shared bathrooms.

They are roughly 500 meters from the venue.

Dormitory rooms will be availble for self-paid accomodation as well.

## Other hotels

If you are looking for a hotel near from the venue, we suggest (in order of
proximity):

There are several hotels at the Carmel Center, which is one bus away:

 * [Dan Panorama](https://www.danhotels.co.il/HaifaHotels/DanPanoramaHaifaHotel)
 * [Dan Carmel](http://www.danhotels.com/HaifaHotels/DanCarmelHaifaHotel/)

There are several somewhat cheaper hotels in the German colony or the
old city, which is a bit farther (by public transport as well), but is
a nice area. Such as:

 * [Port Inn](http://www.portinn.co.il/) ([OSM](https://www.openstreetmap.org/node/332447166))
